import { describe } from "mocha";
import { expect } from "chai";

import Tax from '../src/tax.mjs';

describe('Tax test suite', () => {
    it('Can calculate price after tax', () => {
        const tax = 24;
        const list_price = 1;
        const expect_price_after_tax = 1.24;
        expect(Tax.applyTax(list_price, tax))
            .to
            .equal(expect_price_after_tax);
    });

    it('Can calculate price before tax', () => {
        const tax = 24;
        const total_price = 1.24;
        const expect_price_after_tax = 1;
        expect(Tax.decuctTax(total_price, tax))
            .to
            .equal(expect_price_after_tax)
    });

    it('Can solve tax rate', () => {
        const list_price = 1;
        const total_price = 1.24;
        const tax = 24;
        expect(Tax.figureTaxRate(list_price, total_price))
            .to
            .equal(tax)
    });

});